﻿using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace EasyXaf.Sample.Module.BusinessObjects;

[DefaultClassOptions]
[XafDisplayName("业务规则")]
public class BusinessRule : BaseObject
{
    public string Name
    {
        get => GetPropertyValue<string>(nameof(Name));
        set => SetPropertyValue(nameof(Name), value);
    }

    [VisibleInListView(false)]
    [Size(SizeAttribute.Unlimited)]
    [EditorAlias("RulesEngineEditor")]
    public string Rules
    {
        get => GetPropertyValue<string>(nameof(Rules));
        set => SetPropertyValue(nameof(Rules), value);
    }

    public BusinessRule(Session session)
        : base(session)
    {
    }
}
