﻿using DevExpress.ExpressApp.Blazor.Editors;
using DevExpress.ExpressApp.Blazor.Editors.Adapters;
using DevExpress.ExpressApp.Model;

namespace EasyXaf.LowCode.RulesEngineEditors;

public class RulesEngineEditor : BlazorPropertyEditorBase
{
    public RulesEngineEditor(Type objectType, IModelMemberViewItem model)
        : base(objectType, model)
    {
    }

    protected override IComponentAdapter CreateComponentAdapter()
    {
        return new RulesEngineAdapter(new RulesEngineModel
        {
            Value = PropertyValue as string,
            Height = ((IModelRulesEngineEditor)Model).RulesEngineEditorHeight
        });
    }
}
