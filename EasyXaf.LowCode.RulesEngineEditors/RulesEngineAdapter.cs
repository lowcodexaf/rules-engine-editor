﻿using DevExpress.ExpressApp.Blazor.Components.Models;
using DevExpress.ExpressApp.Blazor.Editors.Adapters;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Utils;
using Microsoft.AspNetCore.Components;

namespace EasyXaf.LowCode.RulesEngineEditors;

public class RulesEngineAdapter : ComponentAdapterBase
{
    public RulesEngineModel Model { get; }

    public override IComponentModel ComponentModel => Model;

    public RulesEngineAdapter(RulesEngineModel model)
    {
        Model = model;
        Model.ValueChanged = EventCallback.Factory.Create<string>(this, value =>
        {
            Model.Value = value;
            RaiseValueChanged();
        });
    }

    public override object GetValue()
    {
        return Model.Value;
    }

    public override void SetValue(object value)
    {
        Model.Value = value as string;
    }

    public override void SetAllowEdit(bool allowEdit)
    {
        Model.IsReadOnly = !allowEdit;
    }

    public override void SetAllowNull(bool allowNull)
    {
    }

    public override void SetDisplayFormat(string displayFormat)
    {
    }

    public override void SetEditMask(string editMask)
    {
    }

    public override void SetEditMaskType(EditMaskType editMaskType)
    {
    }

    public override void SetErrorIcon(ImageInfo errorIcon)
    {
    }

    public override void SetErrorMessage(string errorMessage)
    {
    }

    public override void SetIsPassword(bool isPassword)
    {
    }

    public override void SetMaxLength(int maxLength)
    {
    }

    public override void SetNullText(string nullText)
    {
    }

    protected override RenderFragment CreateComponent()
    {
        return RulesEngineRenderer.Create(Model);
    }
}
