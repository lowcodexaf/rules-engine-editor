using DevExpress.ExpressApp.Blazor.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace EasyXaf.LowCode.RulesEngineEditors;

public partial class RuleNodeAction
{
    [Parameter]
    public string Title { get; set; }

    [Parameter]
    public string IconName { get; set; }

    [Parameter]
    public string IconColor { get; set; }

    [Parameter(CaptureUnmatchedValues = true)]
    public IDictionary<string, object> Attributes { get; set; }

    [Parameter]
    public EventCallback<MouseEventArgs> Click { get; set; }

    [Inject]
    private IImageUrlService ImageUrlService { get; set; }
}