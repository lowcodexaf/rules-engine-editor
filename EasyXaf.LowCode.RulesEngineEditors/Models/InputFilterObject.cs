﻿using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Validation;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

[DomainComponent]
[XafDisplayName("输入过滤")]
public class InputFilterObject : ModelObject
{
    private string _filter;

    [RuleRequiredField]
    [XafDisplayName("过滤")]
    public string Filter
    {
        get => _filter;
        set => SetPropertyValue(ref _filter, value);
    }

    public override IEnumerable<ValidationResult> Validate()
    {
        foreach (var validationResult in base.Validate())
        {
            yield return validationResult;
        }

        if (string.IsNullOrWhiteSpace(Filter))
        {
            yield return new ValidationResult("输入过滤的过滤不能为空", ValidationResultLevel.Error);
        }
    }
}
