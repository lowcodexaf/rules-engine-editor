﻿using DevExpress.ExpressApp.DC;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

public enum ExpressionType
{
    [XafDisplayName("Lambda表达式")]
    Lambda,

    [XafDisplayName("Criteria表达式")]
    Criteria,
}
