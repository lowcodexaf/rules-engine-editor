﻿using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Validation;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

[DomainComponent]
[XafDefaultProperty(nameof(Name))]
public abstract class ParamObject : ModelObject
{
    private string _name;
    private string _description;

    [RuleUniqueValue]
    [RuleRequiredField]
    [XafDisplayName("名称")]
    public string Name
    {
        get => _name;
        set => SetPropertyValue(ref _name, value);
    }

    [XafDisplayName("描述")]
    public string Description
    {
        get => _description;
        set => SetPropertyValue(ref _description, value);
    }

    public override IEnumerable<ValidationResult> Validate()
    {
        foreach (var validationResult in base.Validate())
        {
            yield return validationResult;
        }

        if (string.IsNullOrWhiteSpace(Name))
        {
            yield return new ValidationResult("参数的名称不能为空", ValidationResultLevel.Error);
        }
    }
}
