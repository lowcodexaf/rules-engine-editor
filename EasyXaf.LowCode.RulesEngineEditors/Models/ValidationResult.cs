﻿using DevExpress.ExpressApp.Data;
using DevExpress.ExpressApp.DC;
using System.ComponentModel;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

[DomainComponent]
[XafDisplayName("验证结果")]
public class ValidationResult
{
    public ValidationResult(string message, ValidationResultLevel level)
    {
        Id = Guid.NewGuid();
        Message = message;
        Level = level;
    }

    [Key]
    [Browsable(false)]
    public Guid Id { get; }

    [XafDisplayName("消息")]
    public string Message { get; }

    [XafDisplayName("等级")]
    public ValidationResultLevel Level { get; }
}
