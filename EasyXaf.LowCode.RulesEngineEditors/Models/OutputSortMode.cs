﻿using DevExpress.ExpressApp.DC;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

public enum OutputSortMode
{
    [XafDisplayName("无")]
    None,

    [XafDisplayName("升序")]
    Ascending,

    [XafDisplayName("降序")]
    Descending
}
