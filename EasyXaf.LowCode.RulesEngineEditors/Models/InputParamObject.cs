﻿using DevExpress.ExpressApp.DC;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

[DomainComponent]
[XafDisplayName("输入参数")]
public class InputParamObject : ParamObject
{
    private string _typeName;
    private bool _isCollection;

    [XafDisplayName("类型名称")]
    public string TypeName
    {
        get => _typeName;
        set => SetPropertyValue(ref _typeName, value);
    }

    [XafDisplayName("集合")]
    public bool IsCollection
    {
        get => _isCollection;
        set => SetPropertyValue(ref _isCollection, value);
    }
}
