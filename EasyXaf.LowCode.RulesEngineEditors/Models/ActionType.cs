﻿using DevExpress.ExpressApp.DC;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

public enum ActionType
{
    [XafDisplayName("成功")]
    Success,

    [XafDisplayName("失败")]
    Failure,
}
