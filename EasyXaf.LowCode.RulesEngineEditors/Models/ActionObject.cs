﻿using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using RulesEngine.Models;
using System.ComponentModel;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

[DomainComponent]
[XafDefaultProperty(nameof(Name))]
public abstract class ActionObject : ModelObject
{
    private ActionType _actionType;

    [Browsable(false)]
    [RuleUniqueValue]
    [RuleRequiredField]
    [XafDisplayName("名称")]
    public abstract string Name { get; }

    [XafDisplayName("名称")]
    public abstract string DisplayName { get; }

    [XafDisplayName("类型")]
    public ActionType ActionType
    {
        get => _actionType;
        set => SetPropertyValue(ref _actionType, value);
    }

    [XafDisplayName("描述")]
    [VisibleInDetailView(false)]
    public abstract string Description { get; }

    public abstract ActionInfo ToActionInfo();
}
