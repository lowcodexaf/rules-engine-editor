﻿using DevExpress.ExpressApp.DC;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

[DomainComponent]
public class StartObject : RuleObject
{
    public StartObject()
    {
        RuleDescription = "开始";
    }
}
