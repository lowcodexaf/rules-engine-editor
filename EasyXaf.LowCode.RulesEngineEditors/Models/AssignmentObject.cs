﻿using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Validation;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

[DomainComponent]
[XafDisplayName("赋值操作")]
[XafDefaultProperty(nameof(TargetObject))]
public class AssignmentObject : ModelObject
{
    private string _targetObject;
    private string _propertyName;
    private string _propertyValue;

    [RuleRequiredField]
    [XafDisplayName("目标对象")]
    public string TargetObject
    {
        get => _targetObject;
        set => SetPropertyValue(ref _targetObject, value);
    }

    [RuleRequiredField]
    [XafDisplayName("属性名称")]
    public string PropertyName
    {
        get => _propertyName;
        set => SetPropertyValue(ref _propertyName, value);
    }

    [RuleRequiredField]
    [XafDisplayName("值表达式")]
    public string PropertyValue
    {
        get => _propertyValue;
        set => SetPropertyValue(ref _propertyValue, value);
    }

    public override IEnumerable<ValidationResult> Validate()
    {
        foreach (var validationResult in base.Validate())
        {
            yield return validationResult;
        }

        if (string.IsNullOrWhiteSpace(TargetObject))
        {
            yield return new ValidationResult("赋值操作的目标对象不能为空", ValidationResultLevel.Error);
        }

        if (string.IsNullOrWhiteSpace(PropertyName))
        {
            yield return new ValidationResult("赋值操作的属性名称不能为空", ValidationResultLevel.Error);
        }

        if (string.IsNullOrWhiteSpace(PropertyValue))
        {
            yield return new ValidationResult("赋值操作的值表达式不能为空", ValidationResultLevel.Error);
        }
    }
}
