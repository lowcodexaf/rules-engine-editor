﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Data;
using DevExpress.ExpressApp.DC;
using System.ComponentModel;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

[DomainComponent]
public abstract class ModelObject : NonPersistentEntityObject, IModelObject
{
    private Guid _oid;

    [Key]
    [Browsable(false)]
    public Guid Oid
    {
        get => _oid;
        set => SetPropertyValue(ref _oid, value);
    }

    [Browsable(false)]
    public IEnumerable<ValidationResult> ValidationResults { get; protected set; } = new List<ValidationResult>();

    public event EventHandler ValidationResultsChanged;

    public ModelObject()
    {
        _oid = Guid.NewGuid();
    }

    protected void OnValidationResultsChanged()
    {
        ValidationResultsChanged?.Invoke(this, EventArgs.Empty);
    }

    public virtual IEnumerable<ValidationResult> Validate()
    {
        return Array.Empty<ValidationResult>();
    }
}
