﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

[DomainComponent]
[XafDisplayName("Json")]
public class JsonObject : NonPersistentEntityObject
{
    private string _json;

    [Key]
    [Browsable(false)]
    public string Id { get; } = "Json";

    public string Json
    {
        get => _json;
        set => SetPropertyValue(ref _json, value);
    }
}
