﻿using DevExpress.ExpressApp.DC;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

public enum OutputAggregateOperator
{
    [XafDisplayName("无")]
    None,

    [XafDisplayName("计数")]
    Count,

    [XafDisplayName("求和")]
    Sum,

    [XafDisplayName("求平均")]
    Average,

    [XafDisplayName("最大值")]
    Max,

    [XafDisplayName("最小值")]
    Min,

    [XafDisplayName("第一个")]
    First,

    [XafDisplayName("最后一个")]
    Last
}
