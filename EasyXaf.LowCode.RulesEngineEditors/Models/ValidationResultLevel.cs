﻿using DevExpress.ExpressApp.DC;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

public enum ValidationResultLevel
{
    [XafDisplayName("信息")]
    Info,

    [XafDisplayName("警告")]
    Warn,

    [XafDisplayName("错误")]
    Error
}
