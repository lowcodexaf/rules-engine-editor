﻿namespace EasyXaf.LowCode.RulesEngineEditors.Models;

public interface IModelObject
{
    IEnumerable<ValidationResult> ValidationResults { get; }

    event EventHandler ValidationResultsChanged;

    IEnumerable<ValidationResult> Validate();
}
