﻿using RulesEngine.Models;
using System.Diagnostics.CodeAnalysis;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

[ExcludeFromCodeCoverage]
public class RulesEngineWorkflow : Workflow
{
    public IDictionary<string, object> Properties { get; set; } = new Dictionary<string, object>();

    public RulesEngineWorkflow()
    {
        WorkflowName = Guid.NewGuid().ToString();
    }
}
