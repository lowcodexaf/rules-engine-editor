﻿namespace EasyXaf.LowCode.RulesEngineEditors.Models;

public interface IWorkflowObjectLink
{
    WorkflowObject Workflow { get; set; }
}
