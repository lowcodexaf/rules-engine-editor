﻿using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Validation;
using Newtonsoft.Json;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

[DomainComponent]
[XafDisplayName("输出类型")]
[XafDefaultProperty(nameof(Name))]
public class OutputType : ModelObject
{
    private string _name;
    private OutputSortMode _sortMode;
    private OutputAggregateOperator _aggregateOperator;

    [RuleRequiredField]
    [XafDisplayName("名称")]
    public string Name
    {
        get => _name;
        set => SetPropertyValue(ref _name, value);
    }

    [XafDisplayName("排序模式")]
    [JsonConverter(typeof(OutputSortModeConverter))]
    public OutputSortMode SortMode
    {
        get => _sortMode;
        set => SetPropertyValue(ref _sortMode, value);
    }

    [XafDisplayName("聚合操作符")]
    [JsonConverter(typeof(OutputAggregateOperatorConverter))]
    public OutputAggregateOperator AggregateOperator
    {
        get => _aggregateOperator;
        set => SetPropertyValue(ref _aggregateOperator, value);
    }

    public override IEnumerable<ValidationResult> Validate()
    {
        foreach (var validationResult in base.Validate())
        {
            yield return validationResult;
        }

        if (string.IsNullOrWhiteSpace(Name))
        {
            yield return new ValidationResult("输出类型的名称不能为空", ValidationResultLevel.Error);
        }
    }
}
