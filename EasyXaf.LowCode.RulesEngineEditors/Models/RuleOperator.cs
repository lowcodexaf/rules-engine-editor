﻿using DevExpress.ExpressApp.DC;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

public enum RuleOperator
{
    [XafDisplayName("并且")]
    And,

    [XafDisplayName("或者")]
    Or,
}
