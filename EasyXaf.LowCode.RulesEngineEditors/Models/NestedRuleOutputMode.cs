﻿using DevExpress.ExpressApp.DC;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

public enum NestedRuleOutputMode
{
    [XafDisplayName("全部")]
    All,

    [XafDisplayName("一个")]
    One
}
