﻿using Newtonsoft.Json;

namespace EasyXaf.LowCode.RulesEngineEditors.Models;

public class OutputSortModeConverter : JsonConverter
{
    public override bool CanConvert(Type objectType)
    {
        return objectType == typeof(OutputSortMode);
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        return FromString(reader.Value as string);
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        writer.WriteValue(ToString((OutputSortMode)value));
    }

    public static OutputSortMode FromString(string value)
    {
        return value switch
        {
            "asc" => OutputSortMode.Ascending,
            "desc" => OutputSortMode.Descending,
            _ => OutputSortMode.None
        };
    }

    public static string ToString(OutputSortMode value)
    {
        return value switch
        {
            OutputSortMode.Ascending => "asc",
            OutputSortMode.Descending => "desc",
            _ => "none"
        };
    }
}
