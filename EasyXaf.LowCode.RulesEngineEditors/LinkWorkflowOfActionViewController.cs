﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using EasyXaf.LowCode.RulesEngineEditors.Models;

namespace EasyXaf.LowCode.RulesEngineEditors;

public class LinkWorkflowOfActionViewController : ObjectViewController<DetailView, RuleObject>
{
    protected override void OnViewControlsCreated()
    {
        base.OnViewControlsCreated();

        var viewItem = View.FindItem(nameof(RuleObject.Actions));
        viewItem.ControlCreated += ViewItem_ControlCreated;
    }

    private void ViewItem_ControlCreated(object sender, EventArgs e)
    {
        if (sender is ListPropertyEditor listPropertyEditor)
        {
            var newActionObjectViewController = listPropertyEditor.Frame.GetController<ActionObjectViewController>();
            if (newActionObjectViewController != null)
            {
                newActionObjectViewController.Workflow = ViewCurrentObject.Workflow;
            }
        }
    }
}
