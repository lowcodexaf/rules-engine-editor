﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;

namespace EasyXaf.LowCode.RulesEngineEditors;

public sealed class LowCodeRulesEngineEditorsModule : ModuleBase
{
    protected override void RegisterEditorDescriptors(EditorDescriptorsFactory editorDescriptorsFactory)
    {
        base.RegisterEditorDescriptors(editorDescriptorsFactory);

        editorDescriptorsFactory.RegisterPropertyEditorAlias("RulesEngineEditor", typeof(string), false);
        editorDescriptorsFactory.RegisterPropertyEditor("RulesEngineEditor", typeof(string), typeof(RulesEngineEditor), false);
    }

    public override void ExtendModelInterfaces(ModelInterfaceExtenders extenders)
    {
        base.ExtendModelInterfaces(extenders);
        extenders.Add<IModelPropertyEditor, IModelRulesEngineEditor>();
    }
}