using EasyXaf.LowCode.RulesEngineEditors.Models;
using Microsoft.AspNetCore.Components;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace EasyXaf.LowCode.RulesEngineEditors;

public partial class RuleNodeContainer : IDisposable
{
    [Parameter]
    public bool IsReadOnly { get; set; }

    [Parameter]
    public ObservableCollection<RuleObject> Rules { get; set; }

    [Parameter]
    public EventCallback ContainerChanged { get; set; }

    private void Rules_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
        ContainerChanged.InvokeAsync();
    }

    protected override void OnInitialized()
    {
        base.OnInitialized();

        if (Rules != null)
        {
            Rules.CollectionChanged += Rules_CollectionChanged;
        }
    }

    public void Dispose()
    {
        if (Rules != null)
        {
            Rules.CollectionChanged -= Rules_CollectionChanged;
        }

        GC.SuppressFinalize(this);
    }
}