﻿using DevExpress.ExpressApp.Model;
using System.ComponentModel;

namespace EasyXaf.LowCode.RulesEngineEditors;

public interface IModelRulesEngineEditor
{
    [Category("Layout")]
    [DefaultValue(500)]
    [ModelBrowsable(typeof(RulesEngineEditorVisibilityCalculator))]
    int RulesEngineEditorHeight { get; set; }
}
