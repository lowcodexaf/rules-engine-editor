﻿using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Model.Core;

namespace EasyXaf.LowCode.RulesEngineEditors;

public class RulesEngineEditorVisibilityCalculator : VisibilityCalculatorBase, IModelIsVisible
{
    public bool IsVisible(IModelNode node, string propertyName)
    {
        if (node is IModelMemberViewItem viewItem)
        {
            if (viewItem.PropertyEditorType == typeof(RulesEngineEditor))
            {
                return true;
            }
        }
        return false;
    }
}
