﻿using EasyXaf.LowCode.RulesEngineEditors.UndoManagers.Commands;
using EasyXaf.LowCode.RulesEngineEditors.UndoManagers.Transactions;

namespace EasyXaf.LowCode.RulesEngineEditors.UndoManagers;

public interface IUndoManager : IDisposable
{
    bool IsRedoing { get; }

    bool IsUndoing { get; }

    bool IsLocked { get; }

    bool CanRedo { get; }

    bool CanUndo { get; }

    bool CanCreateTransaction { get; }

    void Redo();

    void Undo();

    void Clear();

    void BeginLock();

    void EndLock();

    ITransaction CreateTransaction(string actionName);

    void CommitTransactions();

    void RollbackTransactions();

    void RegisterCommand(ICommand command);

    event EventHandler StateChanged;
}
