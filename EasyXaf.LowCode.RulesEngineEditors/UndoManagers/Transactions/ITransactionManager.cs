﻿namespace EasyXaf.LowCode.RulesEngineEditors.UndoManagers.Transactions;

public interface ITransactionManager
{
    void CommitTransaction(ICommandTransaction transaction);

    void RollbackTransaction(ICommandTransaction transaction);
}
