﻿namespace EasyXaf.LowCode.RulesEngineEditors.UndoManagers.Transactions;

public class TransactionFactory : ITransactionFactory
{
    public ICommandTransaction CreateTransaction(ITransactionManager transactionManager)
    {
        return new Transaction(transactionManager ?? throw new ArgumentNullException(nameof(transactionManager)));
    }
}
