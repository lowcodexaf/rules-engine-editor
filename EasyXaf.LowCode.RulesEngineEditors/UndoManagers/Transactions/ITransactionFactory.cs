﻿namespace EasyXaf.LowCode.RulesEngineEditors.UndoManagers.Transactions;

public interface ITransactionFactory
{
    ICommandTransaction CreateTransaction(ITransactionManager transactionManager);
}
