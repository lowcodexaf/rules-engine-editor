﻿namespace EasyXaf.LowCode.RulesEngineEditors.UndoManagers.Transactions;

public interface ITransaction : IDisposable
{
    void Commit();

    void Rollback();
}
