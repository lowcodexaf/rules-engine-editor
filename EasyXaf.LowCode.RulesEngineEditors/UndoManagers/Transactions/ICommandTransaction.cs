﻿using EasyXaf.LowCode.RulesEngineEditors.UndoManagers.Commands;

namespace EasyXaf.LowCode.RulesEngineEditors.UndoManagers.Transactions;

public interface ICommandTransaction : ITransaction, ICommand, IEnumerable<ICommand>
{
    string ActionName { get; set; }

    void RegisterCommand(ICommand command);
}
