﻿namespace EasyXaf.LowCode.RulesEngineEditors.UndoManagers.Commands;

public interface ICommand
{
    void Invoke();
}
