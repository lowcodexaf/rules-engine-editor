﻿namespace EasyXaf.LowCode.RulesEngineEditors.UndoManagers.Commands;

public class ActionCommand : ICommand
{
    private readonly Action _selector;

    public ActionCommand(Action selector)
    {
        _selector = selector;
    }

    public void Invoke()
    {
        _selector();
    }
}