﻿namespace EasyXaf.LowCode.RulesEngineEditors.UndoManagers.Commands;

public class ActionCommandException : Exception
{
    public ActionCommandException(string message)
        : this(message, null)
    {
    }

    public ActionCommandException(string message, Exception innerException)
        : base(message, innerException)
    {
    }
}
