﻿namespace EasyXaf.LowCode.RulesEngineEditors.UndoManagers.States;

public enum UndoRedoState
{
    Idle,
    Undoing,
    Redoing,
    Committing,
    RollingBack
}
