﻿namespace EasyXaf.LowCode.RulesEngineEditors.UndoManagers.States;

public interface IStateHost
{
    UndoRedoState State { get; set; }
}
