﻿namespace EasyXaf.LowCode.RulesEngineEditors.UndoManagers.States;

public class StateSwitcher : IDisposable
{
    private readonly IStateHost _owner;
    private readonly UndoRedoState _backup;

    public StateSwitcher(IStateHost target, UndoRedoState state)
    {
        _owner = target ?? throw new ArgumentNullException(nameof(target));
        _backup = _owner.State;
        _owner.State = state;
    }

    public void Dispose()
    {
        _owner.State = _backup;
        GC.SuppressFinalize(this);
    }
}
